﻿using System;
using VkNet;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VkNet.Model;
using VkNet.Model.RequestParams;
using Microsoft.Extensions.DependencyInjection;
using VkNet.AudioBypassService.Extensions;
using System.Net;
using System.IO;
using System.Threading;
using System.Collections.Generic;

namespace VkMusicForm1
{
    public partial class Form1 : Form
    {
        
        static VkApi api;
        static VkNet.Utils.VkCollection<VkNet.Model.Attachments.Audio> audios;
        static List<string> urlList = new List<string>();
        static bool b = false;
        static string pathToDownload;
        public Form1()
        {
            InitializeComponent();
            pathToDownload = ReadFile();
        }

        // Кнопка LogIn
        public void button1_Click(object sender, EventArgs e)
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddAudioBypass();
            api = new VkApi(serviceCollection);
            bool auth = false;
            
            // Авторизация
            try // Обработка неправильного логина или пароля
            {
                api.Authorize(new ApiAuthParams
                {
                    Login = textBox1.Text,
                    Password = textBox2.Text,
                    TwoFactorAuthorization = () =>
                    {
                        return api.Token;
                    }
                });

                auth = true;
                label4.Visible = false;
                groupAuth.Visible = false;
            }
            catch (VkNet.AudioBypassService.Exceptions.VkAuthException)
            {
                label4.Visible = true;
            }
            if (auth != true) {
                return;
            }
            var res = api.Groups.Get(new GroupsGetParams());
            GetAudio();
        }

        // Получаем список аудиозаписей пользователя 
        private void GetAudio()
        {
            audios = api.Audio.Get(new AudioGetParams { Count = 100 });
            int n = 0;
            foreach (var audio in audios)
            {
                checkedListBox1.Items.Add($" {n} > {audio.Artist} - {audio.Title}");
                n += 1;
            }
        }

        // Кнопка Download, скачиваем выбранную музыку по номерам
        private void button3_Click(object sender, EventArgs e)
        {
            int count = checkedListBox1.CheckedItems.Count;
            
            for (int j = 0; j < count; j++)
            {

                int v = Convert.ToInt32(checkedListBox1.CheckedItems[j].ToString()[1].ToString());
                string url = UrlDec(audios[v].Url.ToString());

                //Thread myThread = new Thread(Download);
                Download(url, v);
                
            }
        }

        public void Download(string url, int v)
        {
            WebClient webClient = new WebClient();

            webClient.DownloadProgressChanged += (o, args) => progressBar1.Value = args.ProgressPercentage;
            webClient.DownloadFileCompleted += (o, args) => progressBar1.Value = 100;
            webClient.DownloadFileAsync(new Uri(url), $@"C:\Users\user\Downloads\{ audios[v].Artist}
                - { audios[v].Title}.mp3");
            //string artist = audios[v].Artist.Replace("\\", "");
            //string title = audios[v].Title.Replace("\\", "");
            try
            {
                webClient.DownloadFile(url, $@"{pathToDownload}\{audios[v].Artist} - {audios[v].Title}.mp3");
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Error. The wrong path to the download folder. Change the path in the settings.");
            }
            progressBar1.Value = 0;
        }

        // Парсим url котрый получили и создаём рабочую ссылку 
        public string UrlDec(string vkurl)
        {
            string url;
            string temp = vkurl.Replace("/index.m3u8", ".mp3");

            Regex regex = new Regex(@"/p\w*/");
            Match match = regex.Match(temp);
            string simvol = match.Value;

            regex = new Regex(@"/p\w*/\w*/");
            url = regex.Replace(temp, simvol);

            return url;
        }

        // Кнопка "выделить все песни (Select All)"
        private void button2_Click(object sender, EventArgs e)
        {
            if (!b)
            {
                for (int i = 0; i < checkedListBox1.Items.Count - 1; i++)
                    checkedListBox1.SetItemChecked(i, true);
                b = true;
            }
            else
            {
                for (int i = 0; i < checkedListBox1.Items.Count - 1; i++)
                    checkedListBox1.SetItemChecked(i, false);
                b = false;
            }
        }

        // Выделение песни оним кликом
        private void checkedListBox1_Click(object sender, EventArgs e)
        {
            checkedListBox1.SetItemChecked(checkedListBox1.SelectedIndex, true);
        }

        // Открывает настройки
        private void pathToTheDownloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupSettings.Visible = true;
        }

        // Сохраняет настройки (кномка SaveChenges)
        private void buttonSaveChenges_Click(object sender, EventArgs e)
        {
            WriteFile(textBox4.Text);
            pathToDownload = textBox4.Text;
            groupSettings.Visible = false;
        }

        // Функция записи строки в файл
        void WriteFile(string str)
        {
            using (FileStream fstream = new FileStream($"settings.txt", FileMode.OpenOrCreate))
            {
                // преобразуем строку в байты
                byte[] array = System.Text.Encoding.Default.GetBytes(str);
                // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
            }
        }

        string ReadFile()
        {
            try
            {
                using (FileStream fstream = File.OpenRead($"settings.txt"))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    // декодируем байты в строку
                    string textFromFile = System.Text.Encoding.Default.GetString(array);
                    return textFromFile;
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                groupSettings.Visible = true;
                
                return "";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            groupSettings.Visible = false;
        }
    }
}
